package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// homePage é a função que será chamada quando o endpoin / for acessado por GET
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

// homePage é a função que será chamada quando o endpoin /cnpj/{cnpj} for acessado por GET
func searchCnpj(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cnpj := vars["cnpj"]

	if len(cnpj) != 14 {
		json.NewEncoder(w).Encode(Erro{ErroMensagem: "Verifique o CNPJ informado"})
		return
	}
	resp, err := http.Get("https://www.receitaws.com.br/v1/cnpj/" + cnpj)
	defer resp.Body.Close()
	if err != nil {
		json.NewEncoder(w).Encode(err)
		return
	}
	if resp.StatusCode > 299 || resp.StatusCode < 200 {
		json.NewEncoder(w).Encode(Erro{ErroMensagem: resp.Status})
		return
	}
	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)
	if result == nil {
		json.NewEncoder(w).Encode(
			struct {
				Erro string `json:"error"`
			}{Erro: "Erro ao executar a consulta"},
		)
		return
	}
	var empresa Empresa
	empresa.Cnpj = result["cnpj"].(string)
	empresa.UltimaAtualizacao = result["ultima_atualizacao"].(string)
	empresa.Abertura = result["abertura"].(string)
	empresa.Nome = result["nome"].(string)
	empresa.Fantasia = result["fantasia"].(string)
	empresa.Status = result["status"].(string)
	empresa.Tipo = result["tipo"].(string)
	empresa.Situacao = result["status"].(string)
	empresa.CapitalSocial = result["capital_social"].(string)
	var endereco Endereco
	endereco.Bairro = result["bairro"].(string)
	endereco.Logradouro = result["logradouro"].(string)
	endereco.Numero = result["numero"].(string)
	endereco.Cep = result["cep"].(string)
	endereco.Municipio = result["municipio"].(string)
	endereco.Uf = result["uf"].(string)
	endereco.Complemento = result["complemento"].(string)
	empresa.End = endereco
	var contato Contato
	contato.Telefone = result["telefone"].(string)
	contato.Email = result["email"].(string)
	empresa.Cont = contato
	var atividade []AtividadePrincipal
	atividade = append(atividade, AtividadePrincipal{
		Text: result["atividade_principal"].([]interface{})[0].(map[string]interface{})["text"].(string),
		Code: result["atividade_principal"].([]interface{})[0].(map[string]interface{})["code"].(string),
	})
	empresa.AtividadePrinc = atividade
	e := struct {
		Emp Empresa `json:"empresa"`
	}{
		Emp: empresa,
	}
	json.NewEncoder(w).Encode(e)
}

// validaRequest valida os dados da requisição do endpoint /quote
func validaRequest(result map[string]interface{}) []Erro {
	var erros []Erro

	if result["destinatario"] == nil {
		erros = append(erros, Erro{ErroMensagem: "Destinatário não informado"})
	} else if result["destinatario"].(map[string]interface{})["endereco"] == nil {
		erros = append(erros, Erro{ErroMensagem: "Endereco do destinatário não informado"})
	} else if result["destinatario"].(map[string]interface{})["endereco"].(map[string]interface{})["cep"] == nil {
		erros = append(erros, Erro{ErroMensagem: "O CEP do endereco do destinatário não informado"})
	} else if len(result["destinatario"].(map[string]interface{})["endereco"].(map[string]interface{})["cep"].(string)) != 8 {
		erros = append(erros, Erro{ErroMensagem: "Verifique o CEP informado"})
	}
	if result["volumes"].([]interface{}) == nil {
		erros = append(erros, Erro{ErroMensagem: "Os volumes não foram informado"})
	} else {
		for index, item := range result["volumes"].([]interface{}) {
			if item.(map[string]interface{})["tipo"] == nil {
				erros = append(erros, Erro{ErroMensagem: "O tipo do volume " + fmt.Sprint(index+1) + " não foi informado"})
			} else if item.(map[string]interface{})["tipo"].(float64) < 1 {
				erros = append(erros, Erro{ErroMensagem: "O tipo do volume " + fmt.Sprint(index+1) + " é inválido"})
			}
			if item.(map[string]interface{})["quantidade"] == nil {
				erros = append(erros, Erro{ErroMensagem: "A quantidade do volume " + fmt.Sprint(index+1) + " não foi informada"})
			} else if item.(map[string]interface{})["quantidade"].(float64) <= 0 {
				erros = append(erros, Erro{ErroMensagem: "A quantidade do volume " + fmt.Sprint(index+1) + " é inválida"})
			}
			if item.(map[string]interface{})["altura"] == nil {
				erros = append(erros, Erro{ErroMensagem: "A altura do volume " + fmt.Sprint(index+1) + " não foi informada"})
			} else if item.(map[string]interface{})["altura"].(float64) <= 0 {
				erros = append(erros, Erro{ErroMensagem: "A altura do volume " + fmt.Sprint(index+1) + " é inválida"})
			}
			if item.(map[string]interface{})["largura"] == nil {
				erros = append(erros, Erro{ErroMensagem: "A largura do volume " + fmt.Sprint(index+1) + " não foi informada"})
			} else if item.(map[string]interface{})["largura"].(float64) <= 0 {
				erros = append(erros, Erro{ErroMensagem: "A largura do volume " + fmt.Sprint(index+1) + " é inválida"})
			}
			if item.(map[string]interface{})["comprimento"] == nil {
				erros = append(erros, Erro{ErroMensagem: "O comprimento do volume " + fmt.Sprint(index+1) + " não foi informado"})
			} else if item.(map[string]interface{})["comprimento"].(float64) <= 0 {
				erros = append(erros, Erro{ErroMensagem: "O comprimento do volume " + fmt.Sprint(index+1) + " é inválido"})
			}
			if item.(map[string]interface{})["peso"] == nil {
				erros = append(erros, Erro{ErroMensagem: "O peso do volume " + fmt.Sprint(index+1) + " não foi informado"})
			} else if item.(map[string]interface{})["peso"].(float64) <= 0 {
				erros = append(erros, Erro{ErroMensagem: "O peso do volume " + fmt.Sprint(index+1) + " é inválido"})
			}
			if item.(map[string]interface{})["valor"] == nil {
				erros = append(erros, Erro{ErroMensagem: "O valor do volume " + fmt.Sprint(index+1) + " não foi informado"})
			} else if item.(map[string]interface{})["valor"].(float64) <= 0 {
				erros = append(erros, Erro{ErroMensagem: "O valor do volume " + fmt.Sprint(index+1) + " é inválido"})
			}
		}
	}
	return erros
}

// mmontaRequest onta a estrutura da requisição a ser encaminhada para a API frete rapido
func montaRequest(result map[string]interface{}) Requisicao {
	var vols []Volume
	for _, item := range result["volumes"].([]interface{}) {
		vols = append(vols, Volume{
			Tipo:        item.(map[string]interface{})["tipo"].(float64),
			Quantidade:  item.(map[string]interface{})["quantidade"].(float64),
			Peso:        item.(map[string]interface{})["peso"].(float64),
			Valor:       item.(map[string]interface{})["valor"].(float64),
			Sku:         item.(map[string]interface{})["sku"].(string),
			Altura:      item.(map[string]interface{})["altura"].(float64),
			Largura:     item.(map[string]interface{})["largura"].(float64),
			Comprimento: item.(map[string]interface{})["comprimento"].(float64),
		})
	}
	requisicao := Requisicao{
		CodigoPlataforma: "588604ab3",
		Token:            "c8359377969ded682c3dba5cb967c07b",
		Remet:            Remetente{"17184406000174"},
		Dest: Destinatario{
			TipoPessoa: 1,
			End:        EnderecoCep{result["destinatario"].(map[string]interface{})["endereco"].(map[string]interface{})["cep"].(string)},
		},
		Volumes: vols,
	}
	return requisicao
}

// priceQuote é a função que será chamada quando o endpoin /quote for acessado por POST
func priceQuote(w http.ResponseWriter, r *http.Request) {
	var result map[string]interface{}
	json.NewDecoder(r.Body).Decode(&result)

	erros := validaRequest(result)
	if len(erros) > 0 {
		json.NewEncoder(w).Encode(erros)
		return
	}

	requisicao := montaRequest(result)

	requestByte, _ := json.Marshal(requisicao)
	resp, err := http.Post("https://freterapido.com/api/external/embarcador/v1/quote-simulator", "application/json", bytes.NewReader(requestByte))
	defer resp.Body.Close()
	if err != nil {
		json.NewEncoder(w).Encode(err)
		return
	}
	if resp.StatusCode > 299 || resp.StatusCode < 200 {
		json.NewEncoder(w).Encode(Erro{ErroMensagem: resp.Status})
		return
	}
	var retorno map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&retorno)

	var transportadoras []Transportadora
	for _, item := range retorno["transportadoras"].([]interface{}) {
		transportadoras = append(transportadoras, Transportadora{
			Nome:         item.(map[string]interface{})["nome"].(string),
			Servico:      item.(map[string]interface{})["servico"].(string),
			PrazoEntrega: item.(map[string]interface{})["prazo_entrega"].(float64),
			PrecoFrete:   item.(map[string]interface{})["preco_frete"].(float64),
		})
	}
	t := struct {
		Transp []Transportadora `json:"transportadoras"`
	}{
		Transp: transportadoras,
	}
	json.NewEncoder(w).Encode(t)
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/cnpj/{cnpj}", searchCnpj)
	myRouter.HandleFunc("/quote", priceQuote).Methods("POST")
	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func main() {
	handleRequests()
}
