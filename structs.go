package main

// Empresa struct que sera retornada na funcao searchCep
type Empresa struct {
	Cnpj              string               `json:"cnpj"`
	UltimaAtualizacao string               `json:"ultima_atualizacao"`
	Abertura          string               `json:"abertura"`
	Nome              string               `json:"nome"`
	Fantasia          string               `json:"fantasia"`
	Status            string               `json:"status"`
	Tipo              string               `json:"tipo"`
	Situacao          string               `json:"situacao"`
	CapitalSocial     string               `json:"capital_social"`
	End               Endereco             `json:"endereco"`
	Cont              Contato              `json:"contato"`
	AtividadePrinc    []AtividadePrincipal `json:"atividade_principal"`
}

// Endereco struct que ira compor a struct Empresa
type Endereco struct {
	Bairro      string `json:"bairro"`
	Logradouro  string `json:"logradouro"`
	Numero      string `json:"numero"`
	Cep         string `json:"cep"`
	Municipio   string `json:"municipio"`
	Uf          string `json:"uf"`
	Complemento string `json:"complemento"`
}

// Contato struct que ira compor a struct Empresa
type Contato struct {
	Telefone string `json:"telefone"`
	Email    string `json:"email"`
}

// AtividadePrincipal struct que ira compor a struct Empresa
type AtividadePrincipal struct {
	Text string `json:"text"`
	Code string `json:"code"`
}

// Requisicao struct será usada para montar o objeto que fara a requisicao a API frete rapido
type Requisicao struct {
	CodigoPlataforma string       `json:"codigo_plataforma"`
	Token            string       `json:"token"`
	Remet            Remetente    `json:"remetente"`
	Dest             Destinatario `json:"destinatario"`
	Volumes          []Volume     `json:"volumes"`
}

// Remetente struct que ira compor a struct Requisicao
type Remetente struct {
	Cnpj string `json:"cnpj"`
}

// Destinatario struct que ira compor a struct Requisicao
type Destinatario struct {
	TipoPessoa int         `json:"tipo_pessoa"`
	End        EnderecoCep `json:"endereco"`
}

// EnderecoCep struct que ira compor a struct Destinatario
type EnderecoCep struct {
	Cep string `json:"cep"`
}

// Volume struct que ira compor a struct Requisicao
type Volume struct {
	Tipo        float64 `json:"tipo"`
	Quantidade  float64 `json:"quantidade"`
	Peso        float64 `json:"peso"`
	Valor       float64 `json:"valor"`
	Sku         string  `json:"sku"`
	Altura      float64 `json:"altura"`
	Largura     float64 `json:"largura"`
	Comprimento float64 `json:"comprimento"`
}

// Transportadora struct será usada para montar o objeto organizara as transportadoras retornadas pela API frete rapido
type Transportadora struct {
	Nome         string  `json:"nome"`
	Servico      string  `json:"servico"`
	PrazoEntrega float64 `json:"prazo_entrega"`
	PrecoFrete   float64 `json:"preco_frete"`
}

// Erro struct sera usada para armazenar erros de validacao e ser devolvidos para o usuario
type Erro struct {
	ErroMensagem string `json:"error"`
}
